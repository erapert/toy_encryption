module toy_crypt;

import std.stdio;
import std.string;

/**
	http://en.wikipedia.org/wiki/Caesar_cypher
*/
class Shift {
	static final char[] encrypt (in char shiftBy, in char [] plaintext) {
		char[] cyphertext = plaintext.dup;
		for (auto i = 0; i < cyphertext.length; ++i) {
			cyphertext [i] = cast(char)(cyphertext [i] + shiftBy);
		}
		return cyphertext;
	}

	static final char[] decrypt (in char shiftBy, in char [] cyphertext) {
		char[] plaintext = cyphertext.dup;
		for (auto i = 0; i < cyphertext.length; ++i) {
			plaintext [i] = cast(char)(cyphertext [i] - shiftBy);
		}
		return plaintext;
	}
}

/**
	http://en.wikipedia.org/wiki/Vigenere_cipher
*/
class Vigenere {
	private enum TABLE_CONSTS {
		ROWS = 26,
		COLS = ROWS,
		TOT_SIZE = ROWS * COLS
	}
	private static char [TABLE_CONSTS.ROWS] [TABLE_CONSTS.COLS] table;
	
	static final char [] encrypt (in char [] key, in char [] plaintext) {
		generateTable ();
		
		char [] cyphertext;
		auto ptextLen = plaintext.length;
		cyphertext.length = ptextLen;
		auto cryptKey = keyLenToTextLen (key, ptextLen);
		
		for (auto i = 0; i < ptextLen; ++i) {
			auto row = plaintext [i] - 'a';
			auto col = cryptKey [i] - 'a';
			cyphertext [i] = table [row] [col];
		}
		return cyphertext;
	}
	
	static final char [] decrypt (in char [] key, in char [] cyphertext) {
		generateTable ();
		
		char [] plaintext;
		auto ctextLen = cyphertext.length;
		plaintext.length = ctextLen;
		auto cryptKey = keyLenToTextLen (key, ctextLen);
		
		for (auto i = 0; i < ctextLen; ++i) {
			auto row = cryptKey [i] - 'a';
			auto plainOffset = table [row].indexOf (cyphertext [i]);
			plaintext [i] = table [0] [plainOffset];
		}
		
		return plaintext;
	}
	
	/**
		The key has to repeat to match the length of the text being encrypted/decrypted.
		So this function duplicates the key to the length of the text.
	*/
	private static final char [] keyLenToTextLen (in char [] key, in ulong textLen) {
		char [] newKey;
		newKey.length = textLen;
		auto nkpos = 0;
		auto kpos = 0;
		auto klen = key.length;
		while (nkpos < textLen) {
			newKey [nkpos++] = key [kpos++];
			if (kpos >= klen) { kpos = 0; }
		}
		return newKey;
	}
	
	/**
		Generates and writes the lookup table.
	*/
	private static final void generateTable () {
		auto offset = 0;
		for (auto r = 0; r < TABLE_CONSTS.ROWS; ++r) {
			char val = cast(char) ('a' + offset++);
			for (auto c = 0; c < TABLE_CONSTS.COLS; ++c) {
				table [r] [c] = val++;
				// if too big then wrap around
				if (val > 'z') { val = 'a'; }
			}
		}
	}
	
	static final void print_table () {
		generateTable ();
		for (auto r = 0; r < TABLE_CONSTS.ROWS; ++r) {
			write ("\"");
			for (auto c = 0; c < TABLE_CONSTS.COLS; ++c) {
				writef ("%s", table [r] [c]);
			}
			write ("\"\n");
		}
	}
}

