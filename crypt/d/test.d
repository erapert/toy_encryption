import std.stdio;
import std.getopt;

import toy_crypt;

void main (string [] args) {
	auto ptext = "something something, foo bar baz bizzle!";
	char [] ctext;
	char [] detext;
	
	// caesar cypher
	/*
	ctext = toy_crypt.Shift.encrypt (3, ptext);
	detext = toy_crypt.Shift.decrypt (3, ctext);
	print_texts ("caesar", ptext, ctext, detext);
	*/
	
	// vigenere
	ctext = toy_crypt.Vigenere.encrypt ("foo", "something");
	detext = toy_crypt.Vigenere.decrypt ("foo", ctext);
	print_texts ("vigenere", "something", ctext, detext);
}

void print_texts (in char [] cyphername, in char [] ptext, in char [] ctext, in char [] detext) {
	writefln ("%s\n-----------------------------------", cyphername);
	writefln ("ptext: '%s'", ptext);
	writefln ("ctext: '%s'", ctext);
	writefln ("detext: '%s'", detext);
}
