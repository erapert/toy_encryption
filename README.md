toy_encryption
==============

NON SECURE encryption programs for fun.

The goal is to implement several different encryption algorithms in as many languages as I can and then compare them.
